class AddDeathsToPersonagems < ActiveRecord::Migration
  def change
    add_column :personagems, :deaths, :integer
  end
end
