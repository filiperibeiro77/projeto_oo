class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.float :damage
      t.integer :exp
      t.float :hpMax
      t.float :hpActual
      t.float :defense
      t.boolean :cidade
      t.boolean :atacable
      t.integer :kills
      t.timestamps
    end
  end
end
