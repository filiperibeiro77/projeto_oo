class CreatePersonagems < ActiveRecord::Migration
  def change
    create_table :personagems do |t|
      t.float :damage
      t.integer :exp
      t.float :hpMax
      t.float :hpActual
      t.float :defense
      t.boolean :cidade
      t.boolean :atacable

      t.timestamps
    end
  end
end
