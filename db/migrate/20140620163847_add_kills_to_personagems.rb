class AddKillsToPersonagems < ActiveRecord::Migration
  def change
    add_column :personagems, :kills, :integer
  end
end
