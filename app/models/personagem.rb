class Personagem < ActiveRecord::Base

	belongs_to :user

	after_initialize :default_values
	def default_values
  		self.damage = 15
  		self.hpMax = 50
  		self.hpActual = 50
  		self.defense = 0
  		self.atacable = true
  		self.exp = 0
  		self.cidade = false
  		self.gold = 100
  		self.deaths = 0
  		self.kills = 0
	end


end
