json.array!(@personagems) do |personagem|
  json.extract! personagem, :id, :damage, :exp, :hpMax, :hpActual, :defense
  json.url personagem_url(personagem, format: :json)
end
