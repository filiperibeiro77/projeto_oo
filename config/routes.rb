Rails.application.routes.draw do
  resources :personagems
  resources :users
  resources :chars
  resources :sessions, only: [:new, :create, :destroy]
  root  'welcome#home'


  get "home/logout"
  match '/signup',  to: 'users#new',            via: 'get'
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'get'
  match '/signup',  to: 'users#new',            via: 'get'
  match '/help',    to: 'welcome#help',    via: 'get'
  match '/welcome/about',   to: 'welcome#about',   via: 'get'
  match '/about', to: 'welcome#about', via: 'get'
  match '/contact', to: 'welcome#contact', via: 'get'
  match '/rpg',     to: 'rpg#index', via: 'get'
  match '/rpg/index', to: 'rpg#show', via: 'get'
  match '/rpg/loja',    to: 'rpg#loja', via: 'get'
  match '/rpg/loja/damage', to: 'item#comprardano', via: 'get'
  match '/rpg/atacar', to: 'rpg#targets', via: 'get'
  match '/index', to: 'welcome#index', via: 'get'
  match '/personagems/teste', to: 'personagem#show', via: 'get'
  match '/rpg/targets', to: 'rpg#targets', via: 'get'
  match '/rpg/cidade', to: 'rpg#cidade', via: 'get'

  # root :to => redirect('/welcome')
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
