require 'spec_helper'

describe "personagems/show" do
  before(:each) do
    @personagem = assign(:personagem, stub_model(Personagem,
      :damage => 1.5,
      :exp => 1,
      :hpMax => 1.5,
      :hpActual => 1.5,
      :defense => 1.5
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/1.5/)
    rendered.should match(/1/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
    rendered.should match(/1.5/)
  end
end
