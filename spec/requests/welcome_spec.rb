require 'spec_helper'

describe "Welcome" do

  describe "Help page" do

    it "should have the content 'Projeto de OO'" do
      visit '/welcome/about'
      expect(page).to have_content('Projeto de OO')
    end
  end
end